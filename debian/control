Source: translitcodec
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Edward Betts <edward@4angle.com>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-setuptools
Rules-Requires-Root: no
Standards-Version: 4.6.1
Homepage: https://github.com/claudep/translitcodec
Vcs-Browser: https://salsa.debian.org/python-team/packages/translitcodec
Vcs-Git: https://salsa.debian.org/python-team/packages/translitcodec.git

Package: python3-translitcodec
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: Unicode to 8-bit charset transliteration codec
 This package contains codecs for transliterating ISO 10646 texts into
 best-effort representations using smaller coded character sets (ASCII,
 ISO 8859, etc.).  The translation tables used by the codecs are from
 the 'transtab' collection by Markus Kuhn.
 .
 Three types of transliterating codecs are provided:
 .
 "long", using as many characters as needed to make a natural replacement.
 For example, \u00e4 LATIN SMALL LETTER A WITH DIAERESIS 'ä' will be replaced
 with 'ae'.
 .
 "short", using the minimum number of characters to make a replacement.
 For example, \u00e4 LATIN SMALL LETTER A WITH DIAERESIS 'ä' will be replaced
 with 'a'.
 .
 "one", only performing single character replacements.  Characters that can
 not be transliterated with a single character are passed through unchanged.
 For example, \u2639 WHITE FROWNING FACE '☹' will be passed through unchanged.
